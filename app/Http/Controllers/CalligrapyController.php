<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

use App\Models\Calligrapy;

/**
 *  處理
 *  1.使用者圖片的上傳
 *  2.串接模型生成書法字
 *  3.將使用者圖片與生成書法字進行比對
 *  4.回傳比對結果
 */
class CalligrapyController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        return Inertia::render('Home', ['user' => $user]);
    }

    public function calligraphy(Request $request)
    {
        $user = Auth::user();
        return Inertia::render('calligraphy/Use', ['user' => $user]);
    }

    public function upload(Request $request)
    {   

        // 假設說沒有臨摹圖片上傳回傳404
        if (!$request->hasFile('file')) {
            return response()->json(['message' => 'No file uploaded!'], 400);
        }

        // 檔案上傳處理
        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $path = Storage::put('public/calligrapy', $file); // 儲存檔案並取得路徑

        // 辨識文字處理
        $calli_text = "至";

        // 儲存檔案
        $calligraphy = new Calligrapy();
        $calligraphy->user_id = Auth::user()->id;                        // 儲存用戶 ID
        $calligraphy->email = Auth::user()->email;// 儲存用戶信箱
        $calligraphy->share = false;                      // 是否分享
        $calligraphy->calli_text = $calli_text;           // 儲存辨識文字
        $calligraphy->calli_person = '王羲之';            // 儲存辨識文字作者
        $calligraphy->imitated_img = $path;               // 儲存上傳檔案的路徑
        $calligraphy->save();                             // 儲存資料到資料庫


        return response()->json([
           'calligraphy' => $calligraphy,
        ]);
    }

    public function history(Request $request)
    {
        // 取得現在登入使用者
        $user = Auth::user();
        $calligraphies = Calligrapy::where('user_id', $user->id)->get();
        return Inertia::render('calligraphy/History', ['calligraphies' => $calligraphies, 'user' => $user]);
    }

    public function shareOrNot(Request $request)
    {
        $calligraphy = Calligrapy::find($request->id);
        $calligraphy->share = !$calligraphy->share;
        $calligraphy->save();
        return response()->json([
            'message' => 'successfully!',
        ]);
    }
}
