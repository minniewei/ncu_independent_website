<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

use App\Models\User;

class LoginController extends Controller
{
    public function register(Request $request) {
        $request->validate([
            'username' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = new User();
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();

        // 登入使用者資訊存入暫存
        Auth::login($user);
        $request->session()->put('user', Auth::user());

        return response()->json([
            'message' => 'successfully!',
        ]);
    }

    public function login(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user == null) {
            return response()->json([
                'message' => 'User not found!',
            ], 404);
        }

        if ($user->password != $request->password) {
            return response()->json([
                'message' => 'Password incorrect!',
            ], 400);
        }

        // 登入使用者資訊存入暫存
        Auth::login($user);
        $request->session()->put('user', Auth::user());
        

        return response()->json([
            'message' => 'successfully!',
            'user' => $request->session()->get('user'),
        ]);
    }

    public function logout() {
        Auth::logout();     
        return redirect('/');
    }
}
