<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\CalligrapyController;
use App\Http\Controllers\DisscussionController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['check.auth'])->group(function () {
    # Discussion 頁面
    Route::post('/addComment', [DisscussionController::class, "addComment"])->name('addComment');

    # History 頁面
    Route::post('/shareOrNot', [CalligrapyController::class, "shareOrNot"])->name('shareOrNot');
       
    # 書法上傳頁面
    Route::get('/calligraphy', [CalligrapyController::class, "calligraphy"])->name('calligraphy');
    Route::post('/upload', [CalligrapyController::class, "upload"])->name('upload');
});

# 首頁
Route::get('/', [CalligrapyController::class, "index"])->name('index');

# 註冊頁面
Route::get('/login', function () {return Inertia::render('Login');});
Route::post('/register/send', [LoginController::class, "register"])->name('register');
Route::post('/login/send', [LoginController::class, "login"])->name('login');

# History 頁面
Route::get('/history', [CalligrapyController::class, "history"])->name('history');

# Disscussion 頁面
Route::get('/discussion', [DisscussionController::class, "disscussion"])->name('disscussion');
Route::post('/getComment', [DisscussionController::class, "getComment"])->name('getComment');

# 登出頁面
Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('index');
})->name('logout');