<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   

        /**
         *  src : 臨摹書法字位址
         *  dst : 書法生成模型生成圖片位址
         *  comment : 評論
         */
        Schema::create('calligrapys', function (Blueprint $table) {
            $table->id();                               // 主鍵
            $table->integer('user_id');                 // 用戶 ID
            $table->string('email', 40);                // 用戶信箱
            $table->string('calli_text', 40);           // 臨摹書法字
            $table->string('calli_person', 40);         // 臨摹書法字作者
            $table->boolean('share');                   // 是否公開分享
            $table->text('imitated_img')->nullable();   // 臨摹書法圖片
            $table->text('generated_img')->nullable();  // 生成書法圖片 
            $table->text('comparing_img')->nullable();  // 臨摹與生成書法字重疊比較圖
            $table->float('ssim', 6, 4)->nullable();    // 結構相似性指標 (SSIM)
            $table->float('ahash', 6, 4)->nullable();   // 均值雜湊數值 (Average Hash)
            $table->float('dhash', 6, 4)->nullable();   // 差值雜湊數值 (Differential Hash)
            $table->float('phash', 6, 4)->nullable();   // 感知雜湊數值 (Perceptual Hash)
            $table->timestamps();                       // Laravel 自動加上 created_at 和 updated_at 欄位
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calligrapys');
    }
};
