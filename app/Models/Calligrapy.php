<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calligrapy extends Model
{
    use HasFactory;
    protected $table = 'calligrapys';
    // 允許批量賦值的屬性（可選，避免 MassAssignmentException）
    protected $fillable = [
        'user_id',
        'email',
        'share',
        'file_path',
        'calli_text',
        'calli_person',
    ];

    // 如果需要，你可以定義屬性的型別（可選）
    protected $casts = [
        'share' => 'boolean',
    ];
}
