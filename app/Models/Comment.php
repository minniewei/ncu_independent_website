<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = 'comments';
    // 允許批量賦值的屬性（可選，避免 MassAssignmentException）
    protected $fillable = [
        'calli_id',
        'email',
        'comment',
    ];
}
