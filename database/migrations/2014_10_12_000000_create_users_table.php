<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id(); 
            $table->string('email', 40)->unique();    // 用戶郵件
            $table->string('password');               // 用戶密碼
            $table->string('username', 40);           // 用戶名稱
            $table->integer('authority')->default(0); // 權限（INT），0 代表一般使用者，1 代表管理者
            $table->timestamps();                     // 這會生成 created_at 和 updated_at 欄位
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
