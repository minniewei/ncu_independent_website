<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

use App\Models\Calligrapy;
use App\Models\Comment;

class DisscussionController extends Controller
{
    public function disscussion(Request $request)
    {
        $user = Auth::user();
        $calligraphies = Calligrapy::where('share', 1)->get();
        return Inertia::render('disscussion/Disscussion', ['calligraphies' => $calligraphies, 'user' => $user]);
    }

    public function addComment(Request $request)
    {
        $request->validate([
            'calli_id' => 'required|integer',
            'comment' => 'required|string',
        ]);

        $comment = new Comment();
        $comment->calli_id = $request->calli_id;
        $comment->email = Auth::user()->email;
        $comment->content = $request->comment;
        $comment->save();

        return response()->json([
            'message' => 'successfully!',
        ]);
    }

    public function getComment(Request $request)
    {
        $request->validate([
            'calli_id' => 'required|integer',
        ]);

        $comments = Comment::where('calli_id', $request->calli_id)->get();
        return response()->json($comments);
    }
}
